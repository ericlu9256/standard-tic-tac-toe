from pkg_resources import ContextualVersionConflict


def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, number):
    print("GAME OVER!")
    print(board[number], "has won")
    exit()

def is_row_winner(board, row_number):
    if row_number == 1 and board[0] == board[1] and board[1] == board[2]:
        return True
    elif row_number == 2 and board[3] == board[4] and board[4] == board[5]:
        return True
    elif row_number == 3 and board[6] == board[7] and board[7] == board[8]:
        return True
    else: 
        return False
        
def is_colum_winner(board, col_number):
    if col_number == 1 and board[0] == board[3] and board[3] == board[6]:
        return True
    elif col_number == 2 and board[1] == board[4] and board[4] == board[7]:
        return True
    elif col_number == 3 and  board[2] == board[5] and board[5] == board[8]:
        return True
    else: 
        return False

def is_diagonals_winner(board): 
    if board[0] == board[4] and board[4] == board[8]:
        return True  
    elif board[2] == board[4] and board[4] == board[6]:
        return True
    else:
        return False


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        print_board(board)
        game_over(board, 0)
    elif is_row_winner(board, 2):
        print_board(board)
        game_over(board, 3)
    elif is_row_winner(board, 3):
        print_board(board)
        game_over(board, 6)
    elif is_colum_winner(board, 1):
        print_board(board)
        game_over(board, 0)
    elif is_colum_winner(board, 2):
        print_board(board)
        game_over(board, 1)
    elif is_colum_winner(board, 3):
        print_board(board)
        game_over(board, 2)
    elif is_diagonals_winner(board):
        print_board(board)
        game_over(board, 4)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
